# fetch

Atom Project fetching via GitHub.

### What is fetch?
fetch is a Atom plugin for creating projects via GitHub repositories.
It creates a project from a specific repository you would like to use. And it doesn´t matter, if it´s on your account or a other developer´s account.

### What do i need?
Well.. **This** question is easy to answer.
Atom.

### How do i use it?!
Oh boy.. Srsly?
Just hack on the code..
NO! RTFM**!!!**

### So.. What´s next?
Dude..! Stop asking such stupid questions! >_<

### How do i get support?
Very easy.
Join the Gitter conversation for fetch:
[![Gitter chat](https://badges.gitter.im/fwndev/fetch.png)](https://gitter.im/fwndev/fetch)
