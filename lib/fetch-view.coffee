{View} = require 'atom'

module.exports =
class FetchView extends View
  @content: ->
    @div class: 'fetch overlay from-top', =>
      @div "The Fetch package is Alive! It's ALIVE!", class: "message"

  initialize: (serializeState) ->
    atom.workspaceView.command "fetch:toggle", => @toggle()
    atom.workspaceView.command "fetch:fetch", => @fetch()

  # Returns an object that can be retrieved when package is activated
  serialize: ->

  # Tear down any state and detach
  destroy: ->
    @detach()

  toggle: ->
    if @hasParent()
      @detach()
    else
      atom.workspaceView.append(this)

  fetch: ->
    if @hasParent()
      @detach()
    else
      atom.workspaceView.append(this)
