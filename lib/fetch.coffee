FetchView = require './fetch-view'

module.exports =
  fetchView: null

  activate: (state) ->
    @fetchView = new FetchView(state.fetchViewState)

  deactivate: ->
    @fetchView.destroy()

  serialize: ->
    fetchViewState: @fetchView.serialize()
